const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController")

// route for checking if the urser's email already exists in our database
router.post("/checkEmail", (req,res) =>{
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// routes for user registration
router.post("/register", (req,res) =>{
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

//User Authentication
router.post("/login", (req,res)=> {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// activity step 1 retrieve details of user
router.post("/details", (req,res)=> {
	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController))
});


module.exports = router;





/*

const cutPieces = function (fruit) {
	return fruit * 4;
};

const fruitProcessor = function (apples, oranges) {
	
	const applePieces = cutPieces(apples);
	const orangePieces = cutPieces(oranges);

	const juice = `Juice with ${applePieces} pieces of apple and ${orangePieces} pieces of orange.`;
	return juice;
};

console.log(fruitProcessor(2, 3));




*/